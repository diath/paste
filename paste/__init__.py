from flask import Flask, session, request
from flask_sqlalchemy import SQLAlchemy

from paste.config import *

app = Flask(__name__)
app.secret_key = SECRET_KEY
app.config["SQLALCHEMY_DATABASE_URI"] = DB_URI
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

if not DEBUG:
    import logging

    handler = logging.FileHandler(LOG_FILE)
    handler.setLevel(logging.WARNING)
    app.logger.addHandler(handler)

from paste.models.user import User
from paste.models.entry import Entry

from paste.filters import *
from paste.helpers import *
from paste.converters import *

from paste.views import index, entry, list, api, script, stats, account


@app.before_request
def restore_session():
    user = session.get("user", None)
    if not user and "auth" in request.cookies:
        data = request.cookies.get("auth")
        if data:
            data = data.split(":")
            uid = data[0]
            auth = data[1]

            user = (
                db.session()
                .query(User.id)
                .filter(User.id == uid)
                .filter(User.auth == auth)
                .first()
            )
            if user:
                session["user"] = user.id
