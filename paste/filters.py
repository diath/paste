from datetime import datetime

from paste import app
from paste import DATE_FORMAT


@app.template_filter("datetime")
def filter_datetime(value):
    return datetime.fromtimestamp(int(value)).strftime(DATE_FORMAT)
