from flask import render_template
from sqlalchemy.sql.functions import func
from sqlalchemy.sql.expression import desc

from paste import app, db
from paste import Entry, User


@app.route("/stats")
def route_stats():
    # It's shit but I currently don't know how to do it better.
    users = db.session().query(func.COUNT(User.id)).first()[0]
    total = db.session().query(func.COUNT(Entry.id)).first()[0]
    public = (
        db.session().query(func.COUNT(Entry.id)).filter(Entry.exposure == 0).first()[0]
    )
    private = (
        db.session().query(func.COUNT(Entry.id)).filter(Entry.exposure == 1).first()[0]
    )
    trending = (
        db.session()
        .query(Entry.syntax, func.COUNT(Entry.syntax).label("count"))
        .group_by(Entry.syntax)
        .order_by(desc("count"))
        .limit(5)
        .all()
    )

    return render_template(
        "stats.htm",
        users=users,
        total=total,
        public=public,
        private=private,
        trending=trending,
    )
