from flask import render_template, request, Response
from sqlalchemy import and_

import enum
from time import time
from pygments.lexers import get_all_lexers

from paste import app, db
from paste import Entry, User
from paste import generate_hash

Result = enum.IntEnum(
    "Result",
    "Success InvalidData InvalidSyntax InvalidExpiration InvalidExposure InvalidToken Throttled",
)


@app.route("/api", methods=["GET"])
def route_api():
    return render_template("api.htm")


@app.route("/api", methods=["POST"])
def route_api_post():
    syntax = request.form.get("syntax", "None", type=str)
    exposure = request.form.get("exposure", 0, type=int)
    expiration = request.form.get("expiration", 0, type=int)
    content = request.form.get("content", "", type=str)
    token = request.form.get("token", "", type=str)

    if not content or len(content) > 4 * 1024 * 1024 or not token:
        return "{}:0".format(Result.InvalidData)

    if expiration < 0 or expiration > 604800:
        return "{}:0".format(Result.InvalidExpiration)

    if exposure not in [0, 1]:
        return "{}:0".format(Result.InvalidExposure)

    found = False
    if syntax == "None":
        found = True
    else:
        for language in get_all_lexers():
            if syntax == language[1][0]:
                found = True
                break

    if not found:
        return "{}:0".format(Result.InvalidSyntax)

    token = token.split("-")
    if len(token) < 2:
        return "{}:0".format(Result.InvalidToken)

    uid = token[0]
    token = token[1]

    user = (
        db.session()
        .query(User.id)
        .filter(and_(User.id == uid, User.token == token))
        .first()
    )
    if not user:
        return "{}:0".format(Result.InvalidToken)

    hash = generate_hash()

    entry = Entry()
    entry.syntax = syntax
    entry.exposure = exposure
    entry.expiration = expiration
    entry.content = content
    entry.added = time()
    entry.pid = hash
    entry.author = user.id

    db.session().add(entry)
    db.session().commit()

    return Response("{}:{}".format(Result.Success, hash))
