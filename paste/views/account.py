from flask import (
    render_template,
    request,
    session,
    make_response,
    redirect,
    url_for,
    flash,
    get_flashed_messages,
)
from flask_sqlalchemy import Pagination
from sqlalchemy.sql.functions import func

from time import time
from hashlib import sha1

from paste import app, db
from paste import Entry, User
from paste import login_required, current_user, check_captcha
from paste import SESSION_TIME, CAPTCHA_SITE_KEY


@app.route("/register", methods=["GET"])
def route_register():
    return render_template("register.htm", site_key=CAPTCHA_SITE_KEY)


@app.route("/register", methods=["POST"])
def route_register_post():
    mail = request.form.get("mail", "", type=str)
    pswd = request.form.get("pswd", "", type=str)
    pswdRepeat = request.form.get("pswdRepeat", "", type=str)
    captcha = request.form.get("g-recaptcha-response")
    error = False

    # Muh validation
    if len(mail) < 6:
        flash("The specified email address is not valid.", "error")
        error = True

    if len(pswd) < 5:
        flash("The password must be at least 5 characters long.", "error")
        error = True

    if pswd != pswdRepeat:
        flash("The passwords do not match.", "error")
        error = True

    if check_captcha(captcha) is False:
        flash("The human verification failed.", "error")
        error = True

    user = User.query.filter(User.email == mail).first()
    if user:
        flash("The email address is already in use.", "error")
        error = True

    if error:
        return redirect(url_for("route_register"))

    hash = sha1()
    hash.update(pswd.encode("utf-8"))
    pswd = hash.hexdigest()

    user = User()
    user.email = mail
    user.password = pswd
    user.created = int(time())
    user.nick = ""
    user.admin = 0
    user.token = ""
    user.auth = ""

    db.session().add(user)
    db.session().commit()

    return redirect(url_for("route_login"))


@app.route("/login", methods=["GET"])
def route_login():
    return render_template("login.htm")


@app.route("/login", methods=["POST"])
def route_login_post():
    mail = request.form.get("mail", "", type=str)
    pswd = request.form.get("pswd", "", type=str)

    if len(pswd):
        hash = sha1()
        hash.update(pswd.encode("utf-8"))
        pswd = hash.hexdigest()

    user = User.query.filter(User.email == mail).filter(User.password == pswd).first()
    if user is None:
        flash("The specified email address or password is not valid.", "error")
        return redirect(url_for("route_login"))

    hash = sha1()
    hash.update(
        "".join([str(time()), user.email, str(user.created), str(user.id)]).encode(
            "utf-8"
        )
    )
    auth = hash.hexdigest()

    user.auth = auth
    db.session().commit()

    session["user"] = user.id

    response = make_response(redirect(url_for("route_account")))
    response.set_cookie("auth", "{}:{}".format(user.id, auth), SESSION_TIME)
    return response


@app.route("/logout")
def route_logout():
    session.pop("user")

    response = make_response(redirect(url_for("route_login")))
    response.set_cookie("auth", "", SESSION_TIME)
    return response


@app.route("/account", methods=["GET"])
@login_required
def route_account():
    user = current_user()
    entries = (
        Entry.query.filter(Entry.author == user.id)
        .order_by(Entry.id.desc())
        .limit(20)
        .all()
    )
    return render_template("account.htm", user=user, entries=entries)


@app.route("/account", methods=["POST"])
@login_required
def route_account_post():
    change_pw = request.form.get("changepassword", False, type=bool)
    if change_pw:
        pswdCurrent = request.form.get("pswdCurrent", "", type=str)
        pswd = request.form.get("pswd", "", type=str)
        pswdRepeat = request.form.get("pswdRepeat", "", type=str)
        error = False

        user = current_user()
        hash = sha1()
        hash.update(pswdCurrent.encode("utf-8"))
        if hash.hexdigest() != user.password:
            flash("The current password is incrrect.", "error")
            error = True

        if len(pswd) < 5:
            flash("The password must be at least 5 characters long.", "error")
            error = True

        if pswd != pswdRepeat:
            flash("The passwords do not match.", "error")
            error = True

        if not error:
            hash = sha1()
            hash.update(pswd.encode("utf-8"))

            user = current_user()
            user.password = hash.hexdigest()
            db.session().commit()

            flash("Your password has been changed.", "success")

    change_nick = request.form.get("changenick", False, type=bool)
    if change_nick:
        nick = request.form.get("nick", "", type=str)
        error = False

        if len(nick) < 3:
            flash("Your nick must be at least 3 characters long.", "error")
            error = True

        if len(nick) > 32:
            flash("Your nick can be 32 characters long at most.", "error")
            error = True

        if not error:
            user = current_user()
            user.nick = nick
            db.session().commit()

            flash("Your nickname has been changed.", "success")

    get_token = request.form.get("gettoken", False, type=bool)
    if get_token:
        user = current_user()
        if len(user.token) != 0:
            flash("You already have an API token.", "error")
            return redirect(url_for("route_account"))

        hash = sha1()
        hash.update(
            "".join([str(time()), user.email, str(user.created), str(user.id)]).encode(
                "utf-8"
            )
        )

        user.token = hash.hexdigest()
        db.session().commit()

        flash("Your API token has been generated.", "success")

    return redirect(url_for("route_account"))


@app.route("/delete/<string:pid>", methods=["GET"])
@login_required
def route_delete(pid):
    user = current_user()
    entry = Entry.query.filter(Entry.pid == pid).filter(Entry.author == user.id).first()

    if entry:
        db.session().delete(entry)
        db.session().commit()

        flash("The entry has been deleted.", "success")
    else:
        flash("The entry could not be found or you are not its author.", "error")

    return redirect(url_for("route_account"))


@app.route("/manage", methods=["GET"])
@app.route("/manage/<int:page>", methods=["GET"])
@login_required
def route_manage(page=1):
    if page < 1:
        page = 1

    user = current_user()
    perpage = 10

    entries = (
        Entry.query.filter(Entry.author == user.id)
        .order_by(Entry.id.desc())
        .offset((page - 1) * perpage)
        .limit(perpage)
        .all()
    )
    total = (
        db.session()
        .query(func.COUNT(Entry.id))
        .filter(Entry.author == user.id)
        .first()[0]
    )

    pagination = Pagination(entries, page, perpage, total, entries)
    return render_template("manage.htm", entries=entries, pagination=pagination)
