from flask import render_template

from time import time

from paste import app, db
from paste import Entry
from paste import current_user, author_by_id


@app.route("/list")
def route_list():
    user = current_user()
    is_admin = user and user.admin

    result = (
        db.session()
        .query(
            Entry.pid,
            Entry.syntax,
            Entry.author,
            Entry.added,
            Entry.expiration,
            Entry.exposure,
        )
        .order_by(Entry.id.desc())
    )
    if not is_admin:
        result = result.filter(Entry.exposure == 0).limit(100).all()
    else:
        result = result.limit(250).all()

    entries = []
    for value in result:
        entry = {}
        entry["pid"] = value[0]
        entry["syntax"] = value[1]
        entry["author"] = value[2]
        entry["added"] = value[3]
        entry["expiration"] = value[4]
        entry["exposure"] = value[5]
        entry["expired"] = (
            value.expiration > 0 and (value.added + value.expiration) < time()
        )

        if not is_admin and entry.expired:
            continue

        author = author_by_id(value.author)
        if author is not None:
            entry["nick"] = author.nick
        else:
            entry["nick"] = "Anonymous"

        entries.append(entry)

    return render_template("list.htm", entries=entries)
