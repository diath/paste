from flask import render_template, redirect, url_for, make_response, Response

from pygments import highlight
from pygments.lexers import get_lexer_by_name, get_all_lexers
from pygments.formatters import HtmlFormatter

from time import time
from math import floor

from paste import app, db
from paste import Entry
from paste import current_user
from paste import CAPTCHA_SITE_KEY


@app.route('/<regex("[A-F0-9]{8}"):pid>')
def route_entry(pid):
    entry = db.session().query(Entry).filter(Entry.pid == pid).first()
    if not entry:
        return redirect(url_for("route_index"))

    user = current_user()
    if entry.expired() and not (user and user.admin):
        return redirect(url_for("route_index"))

    if entry.syntax == "None":
        lexer = get_lexer_by_name("text")
        formatter = HtmlFormatter(
            linenos="table", cssclass="source", lineanchors="line", anchorlinenos=True
        )
        content = highlight(entry.content, lexer, formatter)
    else:
        lexer = get_lexer_by_name(entry.syntax)
        formatter = HtmlFormatter(
            linenos="table", cssclass="source", lineanchors="line", anchorlinenos=True
        )
        content = highlight(entry.content, lexer, formatter)

    expiration = 0
    if entry.expiration != 0:
        expiration = floor((entry.added + entry.expiration - time()) / 60)

    return render_template(
        "entry.htm",
        entry=entry,
        content=content,
        expiration=expiration,
        author=entry.getAuthor(),
    )


@app.route('/<regex("[A-F0-9]{8}"):pid>/raw')
def route_entry_raw(pid):
    entry = db.session().query(Entry).filter(Entry.pid == pid).first()
    if not entry:
        return redirect(url_for("route_index"))

    user = current_user()
    if entry.expired() and not (user and user.admin):
        return redirect(url_for("route_index"))

    return Response(entry.content, content_type="text/plain; charset=UTF-8")


@app.route('/<regex("[A-F0-9]{8}"):pid>/simple')
def route_entry_simple(pid):
    entry = db.session().query(Entry).filter(Entry.pid == pid).first()
    if not entry:
        return redirect(url_for("route_index"))

    user = current_user()
    if entry.expired() and not (user and user.admin):
        return redirect(url_for("route_index"))

    if entry.syntax == "None":
        lexer = get_lexer_by_name("text")
        formatter = HtmlFormatter(cssclass="source")
        content = highlight(entry.content, lexer, formatter)
    else:
        lexer = get_lexer_by_name(entry.syntax)
        formatter = HtmlFormatter(cssclass="source")
        content = highlight(entry.content, lexer, formatter)

    return render_template("simple.htm", content=content)


@app.route('/<regex("[A-F0-9]{8}"):pid>/download')
def route_entry_download(pid):
    entry = db.session().query(Entry).filter(Entry.pid == pid).first()
    if not entry:
        return redirect(url_for("route_index"))

    user = current_user()
    if entry.expired() and not (user and user.admin):
        return redirect(url_for("route_index"))

    response = make_response(entry.content)
    response.headers["Content-Disposition"] = "attachment; filename={}".format(pid)
    response.headers["Content-Type"] = "text/plain; charset=UTF-8"

    return response


@app.route('/<regex("[A-F0-9]{8}"):pid>/dup')
def route_entry_dup(pid):
    entry = db.session().query(Entry).filter(Entry.pid == pid).first()
    if not entry:
        return redirect(url_for("route_index"))

    user = current_user()
    if entry.expired() and not (user and user.admin):
        return redirect(url_for("route_index"))

    return render_template(
        "index.htm",
        languages=sorted(get_all_lexers(), key=lambda k: k[0]),
        content=entry.content,
        site_key=CAPTCHA_SITE_KEY,
    )
