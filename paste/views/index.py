from flask import (
    render_template,
    request,
    redirect,
    url_for,
    flash,
    get_flashed_messages,
)
from time import time
from pygments import lexers

from paste import app, db
from paste import Entry
from paste import current_user, generate_hash, check_captcha
from paste import CAPTCHA_SITE_KEY


@app.route("/", methods=["GET"])
def route_index():
    return render_template(
        "index.htm",
        languages=sorted(lexers.get_all_lexers(), key=lambda k: k[0]),
        site_key=CAPTCHA_SITE_KEY,
    )


@app.route("/", methods=["POST"])
def route_index_post():
    syntax = request.form.get("syntax", "None", type=str)
    exposure = request.form.get("exposure", 0, type=int)
    expiration = request.form.get("expiration", 0, type=int)
    content = request.form.get("content", "", type=str)
    captcha = request.form.get("g-recaptcha-response")

    user = current_user()
    lex = lexers.get_all_lexers()
    error = False

    found = False
    if syntax == "None":
        found = True
    else:
        for language in lex:
            if syntax == language[1][0]:
                found = True
                break

    if not found:
        flash("Invalid syntax value.", "error")
        error = True

    if exposure not in [0, 1]:
        flash("Invalid exposure value.", "error")
        error = True

    if expiration not in [0, 1800, 3600, 18000, 43200, 86400, 604800]:
        flash("Invalid expiration value.", "error")
        error = True

    if len(content) == 0:
        flash("The content can not be empty.", "error")
        error = True

    if len(content) > 4 * 1024 * 1024:
        flash("The content size can not exceed 4MB.", "error")
        error = True

    if "viagra" in content:
        flash("Fuck off.", "error")
        error = True

    if not user and CAPTCHA_SITE_KEY:
        if check_captcha(captcha) is False:
            flash("The human verification failed.", "error")
            error = True

    if not error:
        entry = Entry()
        entry.syntax = syntax
        entry.exposure = exposure
        entry.expiration = expiration
        entry.content = content
        entry.added = int(time())
        entry.pid = generate_hash()

        if user:
            entry.author = user.id
        else:
            entry.author = 0

        db.session().add(entry)
        db.session().commit()

        return redirect(url_for("route_entry", pid=entry.pid))

    return redirect(url_for("route_index"))
