from flask import Response

import os

from paste import app


@app.route("/script")
def route_script():
    content = ""
    with open(os.path.join(app.static_folder, "script")) as f:
        content = f.read()

    return Response(content, content_type="text/plain; charset=UTF-8")
