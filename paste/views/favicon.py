from flask import redirect, url_for

from paste import app


@app.route("/favicon.ico")
def route_favicon():
    return redirect(url_for("static", filename="favicon.ico"))
