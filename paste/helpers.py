from flask import session

from functools import wraps
from random import choice
from json import loads
import requests

from paste import db
from paste import Entry, User
from paste import CAPTCHA_SECRET_KEY


def login_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if "user" not in session:
            return redirect(url_for("route_user_login"))

        return f(*args, **kwargs)

    return decorated


def current_user():
    if "user" in session:
        return User.query.filter(User.id == session.get("user", 0)).first()

    return None


def author_by_id(id):
    return User.query.filter(User.id == id).first()


def generate_hash():
    # If this is modified, the entry view route regex needs to be adjusted too.
    bank = "ABCDEF1234567890"

    while True:
        hash = "".join([choice(bank) for x in range(8)])
        entry = db.session().query(Entry.id).filter(Entry.pid == hash).first()
        if not entry:
            return hash


def check_captcha(response):
    try:
        request = requests.get(
            "https://www.google.com/recaptcha/api/siteverify?secret={}&response={}".format(
                CAPTCHA_SECRET_KEY, response
            )
        )

        if request.status_code != 200:
            return False

        data = loads(request.text)
        if not data.get("success"):
            return False

        return True
    except Exception:
        return False
