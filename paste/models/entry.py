from sqlalchemy import Column, Integer, String, Text

from time import time

from paste import db
from paste import User


class Entry(db.Model):
    __tablename__ = "entries"

    # Columns
    id = Column(Integer, primary_key=True, unique=True)
    pid = Column(String(8), unique=True)
    exposure = Column(Integer, default=0)
    added = Column(Integer, default=0)
    expiration = Column(Integer, default=0)
    syntax = Column(String(26), default="None")
    content = Column(Text)
    author = Column(Integer)

    # Methods
    def __init__(self):
        pass

    def __repr__(self):
        return "<Entry.{}.{}>".format(self.id, self.pid)

    def getAuthor(self):
        return User.query.filter(User.id == self.author).first()

    def expired(self):
        return self.expiration > 0 and (self.added + self.expiration) < time()
