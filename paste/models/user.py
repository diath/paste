from sqlalchemy import Column, Integer, String
from paste import db


class User(db.Model):
    __tablename__ = "users"

    # Columns
    id = Column(Integer, primary_key=True, unique=True)
    email = Column(String(64))
    password = Column(String(64))
    nick = Column(String(32))
    created = Column(Integer)
    admin = Column(Integer)
    token = Column(String(32))
    auth = Column(String(64))

    # Methods
    def __init__(self):
        pass

    def __repr__(self):
        return "<User.{}.{}>".format(self.id, self.nick)

    def entries(self):
        return Entry.query.filter(Entry.author == self.id).all()
