# Database URI scheme (Refer to the SQLAlchemy documentation for more variations)
DB_URI = (
    "mysql+pymysql://user:password@host/name?unix_socket=/var/run/mysqld/mysqld.sock"
)

# Secret key
SECRET_KEY = "paste"

# Network host
NET_HOST = "127.0.0.1"

# Network port
NET_PORT = 5000

# Debug mode
DEBUG = True

# Date format
DATE_FORMAT = "%m/%d/%y %I:%M %p"

# Password hashing salt
SALT = "salt"

# Cookie session time
SESSION_TIME = 30 * 24 * 60

# Log file
LOG_FILE = "error.log"

# Captcha Settings (https://www.google.com/recaptcha/intro/index.html)
CAPTCHA_SITE_KEY = ""
CAPTCHA_SECRET_KEY = ""
